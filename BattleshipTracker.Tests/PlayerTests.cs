using System;
using System.Linq;
using BattleshipTracker.Core;
using BattleshipTracker.Core.Enums;
using BattleshipTracker.Core.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BattleshipTracker.Tests
{
    [TestClass]
    public class PlayerTests
    {
        private Game _game;
        [TestInitialize]
        public void Init()
        {
            _game = new Game();
            var p1 = new Player("Player 1");
            var p2 = new Player("Player 2");

            //randomly placing ship
            p1.PlaceShips();
            p2.PlaceShips();

            _game.Player1 = p1;
            _game.Player2 = p2;

        }

        [TestMethod]
        public void Battleship_Player_Attack_Occupied_Ship_Return_Hit()
        {
            
            var panel = _game.Player2.GameBoard.Panels.FirstOrDefault(o => o.IsOccupied);

            if (panel == null) return;
            //take a shot at occupied position
            var shotResult = _game.PlayRound(panel.Coordinates.Row, panel.Coordinates.Column);
            Assert.AreEqual(ShotResult.Hit, shotResult);
        }

        [TestMethod]
        public void Battleship_Player_Attack_Not_Occupied_Ship_Return_Miss()
        {
            var panel = _game.Player2.GameBoard.Panels.FirstOrDefault(o => !o.IsOccupied);

            if (panel == null) return;
            //take a shot at occupied position
            var shotResult = _game.PlayRound(panel.Coordinates.Row, panel.Coordinates.Column);
            Assert.AreEqual(ShotResult.Miss, shotResult);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "Row or Column should not exceed by 10.")]
        public void Battleship_Player_Attack_Exceeded_Board_Return_ArgumentException()
        {
            //take a shot at occupied position
            _game.PlayRound(12, 11);
        }
    }
}
