﻿using BattleshipTracker.Core.Enums;
using BattleshipTracker.Core.Models;
using BattleshipTracker.Core.Models.Boards;

namespace BattleshipTracker.Core
{
    public class Game
    {
        public Player Player1 { get; set; }
        public Player Player2 { get; set; }
        
        public ShotResult PlayRound(int row, int column)
        {
            //Each exchange of shots is called a Round.            
            var coordinates = new Coordinates(row, column);
            
            return Player2.ProcessShot(coordinates);
        }        
    }
}
