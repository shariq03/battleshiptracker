﻿using BattleshipTracker.Core.Enums;

namespace BattleshipTracker.Core.Models.Boards
{
    /// <summary>
    /// The basic class for this modeling practice.  Represents a single square on the game board.
    /// </summary>
    public class Panel
    {
        public OccupationType OccupationType { get; set; }
        public Coordinates Coordinates { get; set; }

        public Panel(int row, int column)
        {
            Coordinates = new Coordinates(row, column);
            OccupationType = OccupationType.Empty;
        }

        public bool IsOccupied => OccupationType == OccupationType.Battleship;
    }
}
