﻿using BattleshipTracker.Core.Enums;

namespace BattleshipTracker.Core.Models.Ships
{
    /// <summary>
    /// Represents a player's ship as placed on their Game Board.
    /// </summary>
    public abstract class Ship
    {
        public string Name { get; set; }
        public int Width { get; set; }
        public OccupationType OccupationType { get; set; }
    }
}
