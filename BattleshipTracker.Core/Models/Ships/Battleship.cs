﻿using BattleshipTracker.Core.Enums;

namespace BattleshipTracker.Core.Models.Ships
{
    public class Battleship : Ship
    {
        public Battleship()
        {
            Name = "Battleship";
            Width = 4;
            OccupationType = OccupationType.Battleship;
        }
    }
}
