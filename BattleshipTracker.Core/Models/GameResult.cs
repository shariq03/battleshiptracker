﻿namespace BattleshipTracker.Core.Models
{
    public class GameResult
    {
        public string AttackMessage { get; set; }
        public string AttackResult { get; set; }

    }
}
