﻿using System;
using System.Collections.Generic;
using System.Linq;
using BattleshipTracker.Core.Enums;
using BattleshipTracker.Core.Extensions;
using BattleshipTracker.Core.Models.Boards;
using BattleshipTracker.Core.Models.Ships;

namespace BattleshipTracker.Core.Models
{
    public class Player
    {
        public string Name { get; set; }
        public GameBoard GameBoard { get; set; }
        public List<Ship> Ships { get; set; }

        public Player(string name)
        {
            Name = name;
            Ships = new List<Ship>
            {
                new Battleship()
            };
            GameBoard = new GameBoard();
        }

        public void PlaceShips()
        {
            var rand = new Random(Guid.NewGuid().GetHashCode());
            foreach (var ship in Ships)
            {
                //Select a random row/column combination, then select a random orientation.
                //If none of the proposed panels are occupied, place the ship
                //Do this for all ships

                var isOpen = true;
                while (isOpen)
                {
                    var startColumn = rand.Next(1, 11);
                    var startRow = rand.Next(1, 11);
                    int endRow = startRow, endColumn = startColumn;
                    var orientation = rand.Next(1, 101) % 2; //0 for Horizontal

                    if (orientation == 0)
                    {
                        for (var i = 1; i < ship.Width; i++)
                        {
                            endRow++;
                        }
                    }
                    else
                    {
                        for (var i = 1; i < ship.Width; i++)
                        {
                            endColumn++;
                        }
                    }

                    //cannot place ships beyond the boundaries of the board
                    if (endRow > 10 || endColumn > 10)
                    {
                        continue;
                    }

                    //Check if specified panels are occupied
                    var affectedPanels = GameBoard.Panels.Range(startRow, startColumn, endRow, endColumn);
                    if (affectedPanels.Any(x => x.IsOccupied))
                    {
                        continue;
                    }

                    foreach (var panel in affectedPanels)
                    {
                        panel.OccupationType = ship.OccupationType;
                    }
                    isOpen = false;
                }
            }
        }

        public ShotResult ProcessShot(Coordinates coords)
        {
            if (coords.Row > 10 || coords.Column > 10)
            {
                throw new ArgumentException("Row or Column should not exceed by 10");
            }

            var panel = GameBoard.Panels.At(coords.Row, coords.Column);
            return !panel.IsOccupied ? ShotResult.Miss : ShotResult.Hit;
        }
    }
}
