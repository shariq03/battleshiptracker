﻿namespace BattleshipTracker.Core.Enums
{
    public enum ShotResult
    {
        Hit,
        Miss
    }
}
