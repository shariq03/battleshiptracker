﻿namespace BattleshipTracker.Core.Enums
{
    public enum OccupationType
    {
        Empty = 'o',
        Battleship = 'B',
        Hit = 'X',
        Miss = 'M'
    }
}
