﻿using BattleshipTracker.Core;
using BattleshipTracker.Core.Models;
using Microsoft.AspNetCore.Mvc;

namespace BattleshipTracker.Api.Controller
{
    [Route("tracker")]
    public class TrackerController : Microsoft.AspNetCore.Mvc.Controller
    {
        private readonly Game _game;
        public TrackerController(Game game)
        {
            _game = game;
        }
        [HttpGet("attack")]
        public IActionResult Attack(int row, int col)
        {

            //1. Create players and their respective game and firing boards
            var p1 = new Player("Player 1");
            var p2 = new Player("Player 2");

            //2. Add ships to the board in random
            p1.PlaceShips();
            p2.PlaceShips();

            _game.Player1 = p1;
            _game.Player2 = p2;

            var result = new GameResult
            {
                AttackMessage = $"{p1.Name} Firing shot at {row}, {col}",
                //3. Take an attack at given position and report back whether it's a hit or a miss
                AttackResult = _game.PlayRound(row, col).ToString()
            };

            return StatusCode(200, result);
        }
    }
}